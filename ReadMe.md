# generate-certificates-in-ci

[![pipeline status](https://gitlab.com/gableroux/generate-certificates-in-ci/badges/master/pipeline.svg)](https://gitlab.com/gableroux/generate-certificates-in-ci/commits/master)

Let's generate self signed certificates inside a CI and that's it

## License

[MIT](LICENSE.md) © [Gabriel Le Breton](https://gableroux.com)


