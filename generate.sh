#!/usr/bin/env sh

set -e
set -x

script_dir="$( cd "$(dirname "$0")" ; pwd -P )"
certificates_destination=${script_dir}/certificates

echo "Generating certificates to ${certificates_destination}"

function generate_certificate() {
  domain=$1
  echo "Generating self-signed certificate for ${domain}"
  cd $script_dir
  echo "SERVER=${domain}" > .env
  docker-compose run generate-certificate
  rm .env
  mkdir ${certificates_destination}/${domain}
  ls -la ${certificates_destination}
  cd ${certificates_destination}
  mv cacert.pem ${domain}/
  mv server.key ${domain}/
  mv server.pem ${domain}/
}

generate_certificate example.com

