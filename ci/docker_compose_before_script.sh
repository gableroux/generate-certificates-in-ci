#!/usr/bin/env sh

set -e
set -x

apk add --update --no-cache \
  py-pip \
  openssh

pip install \
  docker-compose==1.23.2


